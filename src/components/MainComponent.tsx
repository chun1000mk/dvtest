import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import MeuAppBar from './MenuAppBarComponent';
import TabPatient from './TabPatientComponent';
import Tab2DViewer from './Tab2DViewerComponent';
import Tab3DViewer from './Tab3DViewerComponent';

function Main(): React.ReactElement {
  return (
    <div>
      <MeuAppBar />
      <Switch>
        <Route path="/patient" component={TabPatient} />
        <Route exact path="/2dviewer" component={Tab2DViewer} />
        <Route exact path="/3dviewer" component={Tab3DViewer} />
        <Redirect to="/patient" />
      </Switch>
    </div>
  );
}

export default withRouter(Main);
