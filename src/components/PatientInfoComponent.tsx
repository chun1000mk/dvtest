import React from 'react';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';

import { Patient, PatientsState } from '../modules/patient/types';
import usePatients from '../hooks/usePatients';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto',
      maxWidth: 500
    },
    image: {
      width: 128,
      height: 128
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%'
    },
    title: {
      margin: theme.spacing(1, 1, 1)
    },
    patientGrid: {
      margin: theme.spacing(2, 2, 2)
    }
  })
);

function RenderPatientInfo(patient: Patient): React.ReactElement {
  const classes = useStyles();

  if (patient === null || patient === undefined)
    return (
      <div>
        <Typography gutterBottom variant="subtitle1">
          Select a patient from the patient list.
        </Typography>
      </div>
    );

  return (
    <div>
      <Grid container spacing={2} className={classes.patientGrid}>
        <Grid item>
          <ButtonBase className={classes.image}>
            <img className={classes.img} alt="complex" src={patient.image} />
          </ButtonBase>
        </Grid>
        <Grid item xs={4} sm container>
          <Grid item xs container direction="column" spacing={2}>
            <Grid item xs>
              <Typography gutterBottom variant="subtitle1">
                {`Chart No: ${patient.chartno}`}
              </Typography>
              <Typography gutterBottom variant="subtitle1">
                {`Name: ${patient.firstname} ${patient.lastname}`}
              </Typography>
              <Typography gutterBottom variant="subtitle1">
                {`Birthdate: ${patient.birthdate}`}
              </Typography>
              <Typography gutterBottom variant="subtitle1">
                {`Gender: ${patient.gender}`}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default function PatientInfo(): React.ReactElement {
  const ptState: PatientsState = usePatients();
  const classes = useStyles();

  return (
    <div>
      <Typography variant="h6" className={classes.title}>
        Patient Information
      </Typography>
      {RenderPatientInfo(
        ptState.patients.filter(patient => patient.id === ptState.selectedPatient)[0]
      )}
    </div>
  );
}
