import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import PatientInfo from './PatientInfoComponent';
import PatientList from './PatientListComponent';
import ImageList from './ImageListComponent';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary
    }
  })
);

export default function TabPatient(): React.ReactElement {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={0}>
        <Grid item xs={4}>
          <Box height="35vh" border="2px solid #283593" marginTop="0px">
            <PatientInfo />
          </Box>
          <Box height="55vh" border="2px solid #283593" marginTop="0px">
            <PatientList />
          </Box>
        </Grid>
        <Grid item xs={8}>
          <Box height="90.2vh" border="2px solid #283593" marginTop="0px">
            <ImageList />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
