import React from 'react';

import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Typography from '@material-ui/core/Typography';
import usePatients from '../hooks/usePatients';
import { PatientsState } from '../modules/patient/types';
import useImages from '../hooks/useImages';
import { ImagesState } from '../modules/image/types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'flex-start',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
      margin: theme.spacing(1, 1, 1)
    },
    gridList: {
      width: 900
    },
    icon: {
      color: 'rgba(255, 255, 255, 0.54)'
    },
    title: {
      margin: theme.spacing(1, 1, 1)
    }
  })
);

function RenderImageList(): React.ReactNode {
  const classes = useStyles();

  const imgState: ImagesState = useImages();
  const ptState: PatientsState = usePatients();

  let chartNumber: string | null = null;
  if (ptState.selectedPatient !== null) {
    chartNumber = ptState.patients.filter(patient => patient.id === ptState.selectedPatient)[0]
      .chartno;
  }
  const images = imgState.images.filter(image => image.chartno === chartNumber);

  if (ptState.selectedPatient === null) {
    return (
      <div className={classes.root}>
        <p>Image will displayed if a patient is selected.</p>
      </div>
    );
  }

  if (images.length === 0) {
    return (
      <div className={classes.root}>
        <p>There is no image.</p>
      </div>
    );
  }

  return (
    <div className={classes.root}>
      <GridList cellHeight={180} className={classes.gridList} cols={4}>
        {images.map(image => (
          <GridListTile key={image.id}>
            <img src={image.thumbnailUrl} alt={image.createdDate} />
            <GridListTileBar title={image.createdDate} />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}

export default function ImageList(): React.ReactElement {
  const classes = useStyles();

  return (
    <div>
      <Typography variant="h6" className={classes.title}>
        Image List
      </Typography>
      {RenderImageList()}
    </div>
  );
}
