import React, { useEffect } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';

import { PatientsState } from '../modules/patient/types';
import usePatients from '../hooks/usePatients';
import { usePatientActions, useFetchPatient } from '../hooks/usePatientAction';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      maxWidth: 752,
      overflow: 'hide'
    },
    demo: {
      backgroundColor: theme.palette.background.paper
    },
    title: {
      margin: theme.spacing(1, 1, 1)
    }
  })
);

function RenderPatientList(ptState: PatientsState): React.ReactElement {
  const classes = useStyles();
  const [selectedItem, setSelectedItem] = React.useState('');
  const { onSelect } = usePatientActions();

  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    id: string
  ): void => {
    setSelectedItem(id);
    onSelect(id);
  };

  if (ptState.isLoading === true) {
    return <CircularProgress />;
  }
  return (
    <Paper style={{ overflow: 'hide' }}>
      <div className={classes.demo}>
        <List dense>
          {ptState.patients.map(patient => (
            <ListItem
              button
              key={patient.id}
              selected={selectedItem === patient.id}
              onClick={(event): void => handleListItemClick(event, patient.id)}
            >
              <ListItemText primary={`${patient.firstname} ${patient.lastname}`} />
            </ListItem>
          ))}
        </List>
      </div>
    </Paper>
  );
}

export default function PatientList(): React.ReactElement {
  const classes = useStyles();
  const ptState = usePatients();
  const fetchPatient = useFetchPatient();

  useEffect(() => {
    if (ptState.patients.length > 0 || ptState.isLoading === true || ptState.error !== null) return;

    fetchPatient();
  });
  return (
    <div>
      <Typography variant="h6" className={classes.title}>
        Patient List
      </Typography>
      {RenderPatientList(ptState)}
    </div>
  );
}
