import React from 'react';
// import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import MenuAppBar from './MenuAppBarComponent';

test('renders Appbar Title', () => {
  const { getByText } = render(
    <BrowserRouter>
      <MenuAppBar />
    </BrowserRouter>
  );
  const patientElement = getByText(/V-POP Pilot/i);
  expect(patientElement).toBeInTheDocument();
});
