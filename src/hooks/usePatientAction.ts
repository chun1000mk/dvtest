import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import {
  addPatient,
  removePatient,
  selectPatient,
  fetchPatients
} from '../modules/patient/actions';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function useFetchPatient() {
  const dispatch = useDispatch();
  const onLoad = useCallback(() => {
    dispatch(fetchPatients());
  }, [dispatch]);

  return onLoad;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function useAddPatient() {
  const dispatch = useDispatch();
  return useCallback(patient => dispatch(addPatient(patient)), [dispatch]);
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function usePatientActions() {
  const dispatch = useDispatch();

  const onSelect = useCallback(id => dispatch(selectPatient(id)), [dispatch]);
  const onRemove = useCallback(id => dispatch(removePatient(id)), [dispatch]);

  return { onSelect, onRemove };
}
