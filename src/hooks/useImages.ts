import { useSelector } from 'react-redux';
import { RootState } from '../modules/rootReducer';
import { ImagesState } from '../modules/image/types';

export default function useImages(): ImagesState {
  const images = useSelector((state: RootState) => state.images);
  return images;
}
