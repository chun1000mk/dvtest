import { useSelector } from 'react-redux';
import { RootState } from '../modules/rootReducer';
import { PatientsState } from '../modules/patient/types';

export default function usePatients(): PatientsState {
  const patients = useSelector((state: RootState) => state.patients);
  return patients;
}
