import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import Main from './components/MainComponent';
import rootReducer from './modules/rootReducer';
import rootSaga from './sagas/rootSaga';

const App: React.FC = () => {
  const logger = createLogger();
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(rootReducer, applyMiddleware(logger, sagaMiddleware));

  sagaMiddleware.run(rootSaga);

  return (
    <div className="App">
      <Provider store={store}>
        <BrowserRouter>
          <Main />
        </BrowserRouter>
      </Provider>
    </div>
  );
};

export default App;
