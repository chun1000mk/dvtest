import { all, fork } from 'redux-saga/effects';

import watchFetchPatients from './patient';

export default function* rootSaga(): Generator {
  yield all([fork(watchFetchPatients)]);
}
