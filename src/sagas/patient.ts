import { takeEvery, put, delay } from 'redux-saga/effects';
import * as actions from '../modules/patient/actions';
import * as types from '../modules/patient/types';
import INIT_PATIENT from '../shared/patients';

function* fetchPatientsAsync(): Generator {
  try {
    // console.log('fetchPatientAsync');
    yield put(actions.requestPatient());
    yield delay(1000);
    const data = INIT_PATIENT;
    yield put(actions.requestPatientSuccess(data));
  } catch (error) {
    yield put(actions.requestPatientError());
  }
}

export default function* watchFetchPatients(): Generator {
  // console.log('watchFetchPatients');
  yield takeEvery(types.FETCHED_PATIENTS, fetchPatientsAsync);
}
