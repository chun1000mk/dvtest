const INIT_PATIENTS = [
  {
    id: '0',
    chartno: '00000',
    firstname: 'Roxy',
    lastname: 'Austin',
    image: '/assets/patient/Female01.png',
    birthdate: '19900101',
    gender: 'Female'
  },
  {
    id: '1',
    chartno: '11111',
    firstname: 'Firat',
    lastname: 'Morrison',
    image: '/assets/patient/Male01.png',
    birthdate: '19930502',
    gender: 'Male'
  },
  {
    id: '2',
    chartno: '22222',
    firstname: 'Joss',
    lastname: 'Mccoy',
    image: '/assets/patient/Male02.png',
    birthdate: '19640502',
    gender: 'Male'
  },
  {
    id: '3',
    chartno: '33333',
    firstname: 'Lilith',
    lastname: 'Li',
    image: '/assets/patient/Female02.png',
    birthdate: '19930502',
    gender: 'Female'
  },
  {
    id: '4',
    chartno: '44444',
    firstname: 'Saniya',
    lastname: 'Mercado',
    image: '/assets/patient/Female03.png',
    birthdate: '20001222',
    gender: 'Female'
  },
  {
    id: '5',
    chartno: '55555',
    firstname: 'Orlando',
    lastname: 'Livingston',
    image: '/assets/patient/Male03.png',
    birthdate: '19731115',
    gender: 'Male'
  }
];

export default INIT_PATIENTS;
