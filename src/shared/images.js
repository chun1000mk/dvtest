const INIT_IMAGES = [
  {
    id: '0',
    chartno: '00000',
    createdDate: '20200103',
    thumbnailUrl: '/assets/image/images (1).jfif',
    modality: 'Panorama'
  },
  {
    id: '1',
    chartno: '00000',
    createdDate: '20200105',
    thumbnailUrl: '/assets/image/images (2).jfif',
    modality: 'Panorama'
  },
  {
    id: '2',
    chartno: '00000',
    createdDate: '20200106',
    thumbnailUrl: '/assets/image/images (3).jfif',
    modality: 'Panorama'
  },
  {
    id: '3',
    chartno: '00000',
    createdDate: '20180106',
    thumbnailUrl: '/assets/image/images (4).jfif',
    modality: 'Panorama'
  },
  {
    id: '4',
    chartno: '00000',
    createdDate: '20181106',
    thumbnailUrl: '/assets/image/images (5).jfif',
    modality: 'Panorama'
  },
  {
    id: '5',
    chartno: '00000',
    createdDate: '20130723',
    thumbnailUrl: '/assets/image/images (6).jfif',
    modality: 'Panorama'
  },
  {
    id: '6',
    chartno: '11111',
    createdDate: '20190430',
    thumbnailUrl: '/assets/image/images (7).jfif',
    modality: 'Panorama'
  },
  {
    id: '7',
    chartno: '11111',
    createdDate: '20131130',
    thumbnailUrl: '/assets/image/images (8).jfif',
    modality: 'Panorama'
  },
  {
    id: '8',
    chartno: '00000',
    createdDate: '20190430',
    thumbnailUrl: '/assets/image/images (9).jfif',
    modality: 'Panorama'
  },
  {
    id: '9',
    chartno: '33333',
    createdDate: '20130723',
    thumbnailUrl: '/assets/image/images (6).jfif',
    modality: 'Panorama'
  },
  {
    id: '10',
    chartno: '00000',
    createdDate: '20190430',
    thumbnailUrl: '/assets/image/images (7).jfif',
    modality: 'Panorama'
  },
  {
    id: '11',
    chartno: '55555',
    createdDate: '20131130',
    thumbnailUrl: '/assets/image/images (8).jfif',
    modality: 'Panorama'
  },
  {
    id: '12',
    chartno: '55555',
    createdDate: '20190430',
    thumbnailUrl: '/assets/image/images (9).jfif',
    modality: 'Panorama'
  }
];

export default INIT_IMAGES;
