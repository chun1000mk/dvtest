import { combineReducers } from 'redux';
import patients from './patient/reducer';
import images from './image/reducer';

const rootReducer = combineReducers({
  patients,
  images
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
