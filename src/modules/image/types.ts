export const SELECT_IMAGE = 'images/SELECT_IMAGE' as const;

export interface Image {
  id: string;
  chartno: string;
  createdDate: string;
  thumbnailUrl: string;
  modality: string;
}

export interface ImagesState {
  images: Image[];
  selectedImage: string | null;
}

interface SelectImageAction {
  type: typeof SELECT_IMAGE;
  payload: string;
}

export type ImagesActionTypes = SelectImageAction;
