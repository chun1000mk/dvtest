import { ImagesState, ImagesActionTypes, SELECT_IMAGE } from './types';
import INIT_IMAGES from '../../shared/images';

const initialState: ImagesState = {
  images: INIT_IMAGES,
  selectedImage: null
};

function images(state: ImagesState = initialState, action: ImagesActionTypes): ImagesState {
  switch (action.type) {
    case SELECT_IMAGE:
      return { ...state, selectedImage: action.payload };
    default:
      return state;
  }
}

export default images;
