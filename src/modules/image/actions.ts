import { ImagesActionTypes, SELECT_IMAGE } from './types';

function selectImage(id: string): ImagesActionTypes {
  return {
    type: SELECT_IMAGE,
    payload: id
  };
}

export default selectImage;
