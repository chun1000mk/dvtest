import {
  Patient,
  PatientsActionTypes,
  LOADING_PATIENT,
  REQUESTED_PATIENTS,
  REQUESTED_PATIENTS_SUCCEEDED,
  REQUESTED_PATIENTS_FAILED,
  FETCHED_PATIENTS,
  ADD_PATIENTS,
  ADD_PATIENT,
  REMOVE_PATIENT,
  SELECT_PATIENT
} from './types';

export function requestPatient(): PatientsActionTypes {
  return {
    type: REQUESTED_PATIENTS
  };
}

export function requestPatientSuccess(patients: Patient[]): PatientsActionTypes {
  return {
    type: REQUESTED_PATIENTS_SUCCEEDED,
    payload: patients
  };
}
export function requestPatientError(): PatientsActionTypes {
  return {
    type: REQUESTED_PATIENTS_FAILED
  };
}

export function fetchPatients(): PatientsActionTypes {
  return {
    type: FETCHED_PATIENTS
  };
}

export function loadingPatient(): PatientsActionTypes {
  return {
    type: LOADING_PATIENT
  };
}

export function addPatients(patients: Patient[]): PatientsActionTypes {
  return {
    type: ADD_PATIENTS,
    payload: patients
  };
}

export function addPatient(patient: Patient): PatientsActionTypes {
  return {
    type: ADD_PATIENT,
    payload: patient
  };
}

export function removePatient(id: string): PatientsActionTypes {
  return {
    type: REMOVE_PATIENT,
    payload: id
  };
}

export function selectPatient(id: string): PatientsActionTypes {
  return {
    type: SELECT_PATIENT,
    payload: id
  };
}
