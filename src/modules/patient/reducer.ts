import cuid from 'cuid';
import {
  Patient,
  PatientsState,
  PatientsActionTypes,
  REQUESTED_PATIENTS,
  REQUESTED_PATIENTS_SUCCEEDED,
  REQUESTED_PATIENTS_FAILED,
  ADD_PATIENTS,
  ADD_PATIENT,
  REMOVE_PATIENT,
  SELECT_PATIENT
} from './types';

const initialState: PatientsState = {
  patients: [],
  selectedPatient: null,
  isLoading: false,
  error: null
};

function patients(state: PatientsState = initialState, action: PatientsActionTypes): PatientsState {
  switch (action.type) {
    case REQUESTED_PATIENTS:
      return { ...state, isLoading: true };
    case REQUESTED_PATIENTS_SUCCEEDED:
      return { ...state, patients: action.payload, isLoading: false };
    case REQUESTED_PATIENTS_FAILED:
      return { ...state, isLoading: false, error: 'Loading patient is failed!' };
    case ADD_PATIENTS:
      return { ...state, patients: action.payload, isLoading: false };
    case ADD_PATIENT: {
      const nextId = cuid();
      const newPatient: Patient = action.payload as Patient;
      newPatient.id = nextId;
      return { ...state, patients: state.patients.concat(newPatient) };
    }
    case REMOVE_PATIENT: {
      const newState = {
        ...state,
        patients: state.patients.filter(patient => patient.id !== action.payload)
      };
      newState.selectedPatient =
        newState.selectedPatient === action.payload ? null : newState.selectedPatient;
      return newState;
    }
    case SELECT_PATIENT:
      return { ...state, selectedPatient: action.payload };
    default:
      return state;
  }
}

export default patients;
