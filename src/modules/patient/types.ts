export interface Patient {
  id: string;
  chartno: string;
  firstname: string;
  lastname: string;
  image: string;
  birthdate: string;
  gender: string;
}

export interface PatientsState {
  patients: Patient[];
  selectedPatient: string | null;
  isLoading: boolean;
  error: string | null;
}

export const LOADING_PATIENT = 'patients/LOADING_PATIENT' as const;
export const ADD_PATIENTS = 'patients/ADD_PATIENTs' as const;
export const ADD_PATIENT = 'patients/ADD_PATIENT' as const;
export const REMOVE_PATIENT = 'patients/REMOVE_PATIENT' as const;
export const SELECT_PATIENT = 'patients/SELECT_PATIENT' as const;
export const REQUESTED_PATIENTS = 'patients/REQUESTED_PATIENTS' as const;
export const REQUESTED_PATIENTS_SUCCEEDED = 'patients/REQUESTED_PATIENTS_SUCCEEDED' as const;
export const REQUESTED_PATIENTS_FAILED = 'patients/REQUESTED_PATIENTS_FAILED' as const;
export const FETCHED_PATIENTS = 'patients/FETCHED_PATIENTS' as const;

interface RequestedPatientsAction {
  type: typeof REQUESTED_PATIENTS;
}

interface RequestedPatientsSucceededAction {
  type: typeof REQUESTED_PATIENTS_SUCCEEDED;
  payload: Patient[];
}

interface RequestedPatientsFailedAction {
  type: typeof REQUESTED_PATIENTS_FAILED;
}

interface FetchedPatientsAction {
  type: typeof FETCHED_PATIENTS;
}

interface LoadingPatientAction {
  type: typeof LOADING_PATIENT;
}

interface AddPatientsAction {
  type: typeof ADD_PATIENTS;
  payload: Patient[];
}

interface AddPatientAction {
  type: typeof ADD_PATIENT;
  payload: Patient;
}

interface RemovePatientAction {
  type: typeof REMOVE_PATIENT;
  payload: string;
}

interface SelectPatientAction {
  type: typeof SELECT_PATIENT;
  payload: string;
}

export type PatientsActionTypes =
  | RequestedPatientsAction
  | RequestedPatientsSucceededAction
  | RequestedPatientsFailedAction
  | FetchedPatientsAction
  | LoadingPatientAction
  | AddPatientsAction
  | AddPatientAction
  | RemovePatientAction
  | SelectPatientAction;
