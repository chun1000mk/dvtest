import gql from 'graphql-tag';

export const getPatientsQuery = gql`
  query GetPatients {
    allPatient
      {
        _id
        chartno
        firstname
        lastname
        photo_url
        birthdate
        gender
      }
  }
`;

export const getPatientQuery = gql`
  query GetPatient($id: ID!) {
    getPatient(_id: $id)
      {
        _id
        chartno
        firstname
        lastname
        photo_url
        birthdate
        gender
      }
  }
`;