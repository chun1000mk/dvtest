/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetPatients
// ====================================================

export interface GetPatients_allPatient {
  readonly __typename: "Patient";
  readonly _id: string;
  readonly chartno: string;
  readonly firstname: string;
  readonly lastname: string;
  readonly photo_url: string | null;
  readonly birthdate: string;
  readonly gender: string | null;
}

export interface GetPatients {
  readonly allPatient: ReadonlyArray<(GetPatients_allPatient | null)> | null;
}
