/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetPatient
// ====================================================

export interface GetPatient_getPatient {
  readonly __typename: "Patient";
  readonly _id: string;
  readonly chartno: string;
  readonly firstname: string;
  readonly lastname: string;
  readonly photo_url: string | null;
  readonly birthdate: string;
  readonly gender: string | null;
}

export interface GetPatient {
  readonly getPatient: GetPatient_getPatient | null;
}

export interface GetPatientVariables {
  readonly id: string;
}
