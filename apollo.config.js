module.exports = {
  client: {
    includes: ['./src/modules/**/*.tsx', './src/modules/**/*.ts'],
    service: {
      name: "test",
      url: "http://localhost:4000/graphql"
    }
  }
};